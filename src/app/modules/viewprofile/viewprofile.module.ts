import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ViewprofileComponent } from './viewprofile/viewprofile.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [ViewprofileComponent]
})
export class ViewprofileModule { }

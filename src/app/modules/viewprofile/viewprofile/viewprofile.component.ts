import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ServiceService } from '../../../service/service.service';
import { AppComponent } from '../../../app.component';
import { HeaderComponent } from '../../header/header/header.component';
declare var $: any;

@Component({
  selector: 'app-viewprofile',
  templateUrl: './viewprofile.component.html',
  styleUrls: ['./viewprofile.component.css']
})
export class ViewprofileComponent implements OnInit {

    secretKey: any;
    angularxQrCode: string;
    smsOtp: string;
    googleOtp: string;
    smsValue: string;
    gValue: string;
    securityType: string;
    fileData5: any;
    userImage: string;
    cc: any;
    email: any;
    phone: any;
    userName: string;
    google: any;
    sms: any;

    constructor(private router: Router, private spinnerService: Ng4LoadingSpinnerService,private server: ServiceService, private appC: AppComponent, public header: HeaderComponent) { }

    ngOnInit() {
        this.getInfo();
        this.header.getprofile();
        window.scrollTo(0,0);
    }

    editProfile() {
        this.router.navigate(['/header/editprofile']);
    }

    getInfo() {
        this.spinnerService.show();
        this.server.getApi("?key="+localStorage.getItem('token')+"USER").subscribe((succ) => {
            this.spinnerService.hide();
            this.userName = succ[0].firstName;
            this.phone = succ[0].phone;
            this.email = succ[0].email;
            this.cc = succ[0].phoneCountryCode;
            if(succ[0].userImage == null) {
                this.userImage = "assets/images/user-profile.png";
            } else {
                this.userImage = succ[0].userImage;
                this.fileData5 = succ[0].userImage;
            }
            if(succ[0].google2faEnabled == "1") {
            this.securityType = "GA";
            this.google = true;
            } else if(succ[0].smsEnabled == "1") {
            this.securityType = "SMS";
            this.sms = true;
            } else {
            this.securityType = "";
            }
        }, (err) => {
            this.spinnerService.hide();
        });
    }

    switchAuth(type) {
        if(type == this.securityType) {
        let name = "";
        type == "GA" ? name = "Google Authentication" : name = "SMS Verification";
        this.appC.showErrToast("Already using "+name);
        this.getInfo();
        return;
    } else {
            type == "GA" ? (this.gValue="1",this.smsValue="0") : (this.gValue="0",this.smsValue="1")
            if(this.smsValue == "1") {
                this.sms = false;
                this.google = false;
                this.googleOtp = ""
                $("#key").modal({ backdrop: 'static', keyboard: false });
            } else if(this.gValue == "1") {
                this.sms = false;
                this.google = false;
                this.smsOtpGenerate();
            }
    }
    }

    googleVerify() {
        let data = {
        "eventExternal": {
            "name": "request_google_verify",
            "key": "mykey"
        },
        "transferObjectMap": {
            "gatewayrequest": {
                "token": localStorage.getItem("token"),
                "otp": this.googleOtp,
                "secretKey": localStorage.getItem("secretKey"),
                "clientTime": new Date().getTime()
            }
        }
    }
    this.spinnerService.show();
    this.server.postApi('', data).subscribe(res => {
        this.spinnerService.hide();
        if (res.transferObjectMap.statusCode == 200) {
            this.appC.showSuccToast(res.transferObjectMap.status);
            $("#key").modal('hide');
            if(this.gValue == '1'){
                this.sms = false;
                this.userSetting();  
            } else {
                this.smsOtpGenerate();
            }
        } else {
            this.appC.showErrToast(res.transferObjectMap.message);
        }
    }, error => {
        this.spinnerService.hide();
    });
    }

    smsOtpGenerate() {
        this.smsOtp = "";
        let data = {
            "eventExternal": {
                "name": "request_sms_auth",
                "key": "mykey"
            },
            "transferObjectMap": {
                "gatewayrequest": {
                    "token": localStorage.getItem("token")
                }
            }
        }
        this.spinnerService.show();
        this.server.postApi('', data).subscribe(res => {
            this.spinnerService.hide();
            if (res.transferObjectMap.statusCode == 200) {
            this.appC.showSuccToast(res.transferObjectMap.status);
                // this.resendButton = false;
                $("#sms-verify").modal({ backdrop: 'static', keyboard: false });
                // this.timer();
            } else {
            this.appC.showErrToast(res.transferObjectMap.message);
            }
        }, error => {
                this.spinnerService.hide()
        });
    }

    smsVerify() {
        let data = {
            "eventExternal": {
                "name": "request_sms_verify",
                "key": "mykey"
            },
            "transferObjectMap": {
                "gatewayrequest": {
                    "code": this.smsOtp,
                    "token": localStorage.getItem("token")
                }
            }
        }
        this.spinnerService.show();
        this.server.postApi('', data).subscribe(res => {
            this.spinnerService.hide()
            if (res.transferObjectMap.statusCode == 200) {
                this.appC.showSuccToast(res.transferObjectMap.status);
                $("#sms-verify").modal('hide');
                if(this.smsValue == '1') {
                this.google = false;
                this.userSetting()
            }
            else {
                this.googleOtpGenerate()
            } 
            }
            else {
            this.appC.showErrToast(res.transferObjectMap.message);
            }
        }, error => {
                this.spinnerService.hide()
        });

    }

    googleOtpGenerate() {
        // $('#key').modal('show')
        this.googleOtp = "" 
        let data = {
            "eventExternal": {
                "name": "request_google_auth",
                "key": "mykey"
            },
            "transferObjectMap": {
                "gatewayrequest": {
                    "token": localStorage.getItem("token")
                }
            }
        }
        this.spinnerService.show();
        this.server.postApi('', data).subscribe(res => {
            this.spinnerService.hide();
            if (res.transferObjectMap.statusCode == 200) {
                this.appC.showSuccToast(res.transferObjectMap.status);
                this.angularxQrCode="data:image/png;base64,"+res.transferObjectMap.QRData
                this.secretKey = res.transferObjectMap.secretKey;
                localStorage.setItem("secretKey",res.transferObjectMap.secretKey);
                $("#key").modal({ backdrop: 'static', keyboard: false });
            } else {
                this.appC.showErrToast(res.transferObjectMap.message);
            }
        }, error => {
            this.spinnerService.hide();
        });

    }

    userSetting() {
        let data = {
            "eventExternal": {
                "name": "request_user_setting",
                "key": "mykey"
            },
            "transferObjectMap": {
                "gatewayrequest": {
                        "google2faEnabled": this.gValue,
                        "smsEnabled": this.smsValue,
                        "token": localStorage.getItem('token')
                }
            }
        }
        this.spinnerService.show();
        this.server.postApi('',data).subscribe((succ) => {
            this.spinnerService.hide();
            if(succ.transferObjectMap.statusCode == 200) {
                this.appC.showSuccToast("2FA changed successfully.");
                this.getInfo();
            } else if(succ.transferObjectMap.statusCode == 403) {
                this.header.logout();
            }
        }, (err) => {
            this.spinnerService.hide();
        });
    }
    
    /**To resend otp */
    resend(){
    /**otp verfication  */
    let smsNo = {
        "eventExternal": {
            "name":"request_sms_auth",
            "key":"mykey"
        },
        "transferObjectMap": {
            "gatewayrequest": {
                "phoneCountryCode": "",
                "phone": "",
                "token":localStorage.getItem('token')
            }
        }   
    }
    this.spinnerService.show();
    this.server.postApi('', smsNo).subscribe(response => {
        this.spinnerService.hide();
        if (response.transferObjectMap.statusCode == 200) {
            this.appC.showSuccToast(response.transferObjectMap.message);
            this.smsOtp = '';
        } else if (response.transferObjectMap.statusCode == 403) {
            this.header.logout();
        } else {
            this.smsOtp = '';
            this.appC.showErrToast(response.transferObjectMap.message);
        }
    }, error => {
        this.appC.showErrToast('Something went wrong');
        this.spinnerService.hide();
    });
    }

}

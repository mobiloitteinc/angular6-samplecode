import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ServiceService } from '../../../service/service.service';
import { AppComponent } from '../../../app.component';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { HeaderComponent } from '../../header/header/header.component';
import { FormGroup, FormControl, Validators } from '@angular/forms';

@Component({
  selector: 'app-editprofile',
  templateUrl: './editprofile.component.html',
  styleUrls: ['./editprofile.component.css']
})

export class EditprofileComponent implements OnInit {
    userName: string;
    phone: any;
    email: any;
    cc: any;
    userImage: string;
    fileData5: any;
    userDetailForm: FormGroup;
    countryList = [];
    selectedCountry = "";
    selectCity: string;
    statelist = [];
    selectState = {'state':''};
    minAge: Date;
    authObj: any = {'docFile':''};
    fileName: any;
    fileData: any;
    myImage: any;
    check: number=0;

    constructor(private router: Router,private server: ServiceService, private appC: AppComponent, private spinnerService: Ng4LoadingSpinnerService, public header: HeaderComponent) { }

    ngOnInit() {
      this.getInfo();
      this.checkInput();
      this.getCountryList();
      var today = new Date();
      var minAge = 0;
      this.minAge = new Date(today.getFullYear() - minAge, today.getMonth(), today.getDate());
      window.scrollTo(0,0);
    }

    getInfo() {
        //this.spinnerService.show();
        this.server.getApi("?key="+localStorage.getItem('token')+"USER").subscribe((succ) => {
            this.spinnerService.hide();
            this.userName = succ[0].firstName;
            this.phone = succ[0].phone;
            this.email = succ[0].email;
            this.cc = succ[0].phoneCountryCode+"-"+succ[0].phone;
            this.selectState = succ[0].state;
            if(succ[0].userImage == null) {
                this.fileData = "assets/images/user-profile.png";
            } else {
                this.fileData = succ[0].userImage;
                this.fileData5 = succ[0].userImage;
            }
            this.userDetailForm.patchValue({
                name: this.userName,
                dob: succ[0].dateOfBirth,
                country: succ[0].country,
                state: succ[0].state,
                email: succ[0].email,
                gender: succ[0].gender,
            })
            if(succ[0].country == null) {
                this.selectedCountry = "";
                this.selectCity = "";
            } else {

                this.selectedCountry = succ[0].country;
                this.selectState = succ[0].state;
                console.log(this.selectState);
            }
            let data = {
                "eventExternal": {
                    "name":"request_statebycountrylist",
                    "key":"mykey"
                },
                "transferObjectMap": {
                    "gatewayrequest": {
                        "countryName": succ[0].country
                    }
                }
            }
            this.spinnerService.show();
            this.server.postApi('',data).subscribe((succ) => {
                this.spinnerService.hide();
                if(succ.transferObjectMap.statusCode == 200) {
                    this.statelist = succ.transferObjectMap.result;
                } else {
                }
            }, (err) => {
                this.spinnerService.hide();
            });
        }, (err) => {
            this.spinnerService.hide();
        });
    }

    /** Function for edit from validation */
    checkInput() {
      this.userDetailForm = new FormGroup({
          name: new FormControl('', [Validators.required, Validators.minLength(2)]),
          gender: new FormControl('', [Validators.required]),
          dob: new FormControl('', [Validators.required]),
          country: new FormControl('', [Validators.required]),
          state: new FormControl('', [Validators.required])
      });
    }
    
    get name(): any {
      return this.userDetailForm.get('name');
    }
    get gender(): any {
      return this.userDetailForm.get('gender');
    }
    get dob(): any {
      return this.userDetailForm.get('dob');
    }
    get country(): any {
      return this.userDetailForm.get('country');
    }
    get state(): any {
      return this.userDetailForm.get('state');
    }

    /** Function to get country list */
    getCountryList() {
        let data = {
            "eventExternal": {
                "name":"request_countrylist",
                "key":"mykey"
            },
            "transferObjectMap": {
                "gatewayrequest":{
                }
            }
        }
        this.spinnerService.show();
        this.server.postApi('',data).subscribe((succ) => {
            this.spinnerService.hide();
            if(succ.transferObjectMap.statusCode == 200) {
                this.countryList = succ.transferObjectMap.result;
            } else {
                
            }
        }, (err) => {
            this.spinnerService.hide();
        });
    }

    /** Function to get city list according to country */
    selectCountry(country) {
        this.check = 1;
        this.userDetailForm.patchValue({
            state: ''
        })
        let data = {
            "eventExternal": {
                "name":"request_statebycountrylist",
                "key":"mykey"
            },
            "transferObjectMap": {
                "gatewayrequest": {
                    "country": country.value
                }
            }
        }
        this.spinnerService.show();
        this.server.postApi('',data).subscribe((succ) => {
            this.spinnerService.hide();
            if(succ.transferObjectMap.statusCode == 200) {
                this.statelist = succ.transferObjectMap.result;
            } else {
            }
        }, (err) => {
            this.spinnerService.hide();
        });
    }

    /** Function to submit updated detail */
    updateProfile() {
      if(new Date(this.userDetailForm.value.dob) > this.minAge) {
          this.appC.showErrToast("Future date is not acceptable.")
      } else {
          // if(this.fileData5 == 'assets/images/user-avatar.png') {
          //     this.fileData5 = "";
          // }
          // if(this.fileData5 != this.userImage) {
          //     if(this.jpeg == true) {
          //         this.fileData5 = this.fileData5.substr(23);
          //     } else {
          //         this.fileData5 = this.fileData5.substr(22);
          //     }
          // } else if(this.fileData5 == this.userImage) {
          //     this.fileData5 = '';
          // }
            if(this.fileData.includes('https'))
                this.myImage = ''
            else if(this.fileData.includes('assets'))
                this.myImage = ''
            let data = {
              "eventExternal": {
                   "name":"request_profile_update",
                  "key":"mykey"
              },
              "transferObjectMap": {
                  "gatewayrequest": {
                      "firstName":this.userDetailForm.value.name,
                      "dateOfBirth":this.userDetailForm.value.dob,
                      "country":this.userDetailForm.value.country,
                      "state":this.userDetailForm.value.state,
                      "gender":this.userDetailForm.value.gender,
                      "userImage":this.myImage,
                      "token":localStorage.getItem("token")
                  }
              }
          }
          for (let val in data) {
              for(let num in data[val]) {
                for(let con in data[val][num]) {
                  if (data[val][num][con] == '') {
                      delete data[val][num][con]
                  }
                }
              }
          }
          this.spinnerService.show();
          this.server.postApi('', data).subscribe(res =>{
              this.spinnerService.hide()
              if(res.transferObjectMap.statusCode == 200) {
                  this.appC.showSuccToast(res.transferObjectMap.message)
                  // this.edit = "false";
                  this.header.getprofile();
              } else {
                  this.appC.showErrToast(res.transferObjectMap.message);
              }
          }, error => {
              this.spinnerService.hide()
          }); 
      }        
    }

    /**Function to update profile pic */
    profilePic(event) {
        var self = this;
        if(event.target.files && event.target.files[0]){
            var type = event.target.files[0].type;
            if(type === 'image/png' || type === 'image/jpg') {
                self.authObj.docFile = event.target.files[0].name;
                this.fileName = self.authObj.docFile;
                var reader = new FileReader()
                reader.onload = (e) =>  {
                    self.fileData = e.target['result'];
                    self.myImage = e.target['result'].substring(22);
                }
                reader.readAsDataURL(event.target.files[0]);
            } else if(type === 'image/jpeg') {
                self.authObj.docFile = event.target.files[0].name;
                this.fileName = self.authObj.docFile;
                var reader = new FileReader()
                reader.onload = (e) =>  {
                    self.fileData = e.target['result'];
                    self.myImage = e.target['result'].substring(23);
                }
                reader.readAsDataURL(event.target.files[0]);
            } else {
                this.appC.showErrToast("Please select png, jpg and jpeg image format.");
                self.authObj.docFile = "";
                
            }
        }
    }

}

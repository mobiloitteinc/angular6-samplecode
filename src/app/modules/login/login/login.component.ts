import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { ServiceService } from '../../../service/service.service';
import { AppComponent } from '../../../app.component';
import { HeaderComponent } from '../../header/header/header.component';
declare var $: any;

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  smsOtp: string;
  googleOtp: string;
  loginForm: FormGroup;
  forgotPasswordForm: FormGroup;
  token: any;

    constructor(private router: Router, private spinnerService: Ng4LoadingSpinnerService,private server: ServiceService, private appC: AppComponent, public header: HeaderComponent) { }

    ngOnInit() {
      window.scrollTo(0,0);
      this.checkInputs();
      this.validation();
    }

    /** Function to validate form inputs */
    checkInputs() {
      this.loginForm = new FormGroup({
          email: new FormControl('', [Validators.required, Validators.pattern(/^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,5})+$/i)]),
          password: new FormControl('', [Validators.required]),          
      })
    }

    validation() {
      this.forgotPasswordForm = new FormGroup({
        forgotPasswordEmail: new FormControl('', [Validators.required, Validators.pattern(/^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,5})+$/i)])
      })
    }

    /** to get the value of field  */
    get email(): any {
      return this.loginForm.get('email');
    }
    get password(): any {
        return this.loginForm.get('password');
    }
    get forgotPasswordEmail(): any {
      return this.forgotPasswordForm.get('forgotPasswordEmail');
    }

    signup() {
      this.router.navigateByUrl('header/signup')
    }

    openForgotPassword() {
      this.forgotPasswordForm.reset();
      $('#forgot-pass').modal('show');
    }

    submitForgotPassword() {
      let data = {
        "eventExternal": {
            "name":"request_forgetpassword",
            "key":"mykey"
        },
        "transferObjectMap": {
          "gatewayrequest": {
            "email": this.forgotPasswordForm.value.forgotPasswordEmail,
            //"url": "http://172.16.6.212:4200/header/reset",
            "url": "http://162.222.32.20:1487/header/reset"
          }
        }
      }
      this.spinnerService.show();
      this.server.postApi('', data).subscribe((res) => {
        this.spinnerService.hide();
        if(res.transferObjectMap.statusCode == 200) {
          this.appC.showSuccToast(res.transferObjectMap.message);
          $('#forgot-pass').modal('hide');
        } else {
          this.appC.showErrToast(res.transferObjectMap.message);
        }
      }, (err) => {
        this.spinnerService.hide();
      })
    }

    login() {
      let data = {
        "eventExternal": {
            "name":"request_login",
            "key":"mykey"
        },
        "transferObjectMap": {
          "gatewayrequest": {
            "email": this.loginForm.value.email ,
             "password": this.loginForm.value.password
          }
        }
    }
    this.spinnerService.show();
    this.server.postApi('',data).subscribe((res) => {
      this.spinnerService.hide();
      if(res.transferObjectMap.statusCode == 200) {
        this.token = res.transferObjectMap.loginToken;
        //localStorage.setItem("token",res.transferObjectMap.loginToken);
        if(res.transferObjectMap.isVerified == null || res.transferObjectMap.isVerified == "0") {
            localStorage.setItem("email",this.loginForm.value.email);
            this.appC.showErrToast(res.transferObjectMap.message);
            this.router.navigate(['/header/emailVerify/login']);
        } else if(res.transferObjectMap.isVerified == "2") {
            localStorage.setItem("userID",res.transferObjectMap.result[0].userId);
            let data = {
              messageType:"userUpdated",
              token: this.token
            }
            this.server.wsExchange.send(JSON.stringify(data));
            if(res.transferObjectMap.result[0].google2faEnabled == null && res.transferObjectMap.result[0].smsEnabled == null ) {
                this.appC.showErrToast("Authentication is mandatory.")
                localStorage.setItem("email",this.loginForm.value.signinemail);
                localStorage.setItem("noAuth","true")
                this.router.navigate(['/header/twofa/'+ this.token])
            } else if(res.transferObjectMap.result[0].google2faEnabled == "1") {  
                this.googleOtp = ""              
                $("#key").modal({ backdrop: 'static', keyboard: false });
            } else if(res.transferObjectMap.result[0].smsEnabled == "1") {
                this.smsAuth();
            } else {
                this.appC.showSuccToast("Logged in successfully.");
                // localStorage.setItem("header",JSON.stringify(true));
                localStorage.setItem("token",res.transferObjectMap.loginToken);
                this.header.loginToken = false;  
            } 
          }      
        } else {
          this.appC.showErrToast(res.transferObjectMap.message);
        }
    }, (err) => {
      this.spinnerService.hide();
    })
      // this.router.navigate(['/header/viewprofile']);
    }

    googleVerify() {
      let data = {
        "eventExternal": {
            "name": "request_google_verify",
            "key": "mykey"
        },
        "transferObjectMap": {
            "gatewayrequest": {
                "token": this.token,
                "otp": this.googleOtp,
                "secretKey": localStorage.getItem("secretKey"),
                "clientTime": new Date().getTime()
            }
        }
    }
    this.spinnerService.show();
    this.server.postApi('', data).subscribe(res => {
      this.spinnerService.hide();
      if (res.transferObjectMap.statusCode == 200) {
            localStorage.setItem('token', this.token);
            localStorage.setItem("header",JSON.stringify(true));
            this.header.loginToken = false;
            this.appC.showSuccToast(res.transferObjectMap.status);
            $("#key").modal('hide');
            let data = {
              messageType:"userUpdated",
              token: this.token
            }
            this.server.wsExchange.send(JSON.stringify(data));
            this.router.navigate(['/header/viewprofile']);
        } else {
          this.appC.showErrToast(res.transferObjectMap.message);
        }
    }, error => {
        this.spinnerService.hide();
    });
    }

    smsAuth() {
      this.smsOtp = "";
      let data = {
          "eventExternal": {
              "name": "request_sms_auth",
              "key": "mykey"
          },
          "transferObjectMap": {
              "gatewayrequest": {
                  "token": this.token,
              }
          }
      }
      this.spinnerService.show();
      this.server.postApi('', data).subscribe(res => {
          this.spinnerService.hide();
          if (res.transferObjectMap.statusCode == 200) {
            this.appC.showSuccToast(res.transferObjectMap.status);
              // this.resendButton = false;
              $("#sms-verify").modal({ backdrop: 'static', keyboard: false });
              // this.timer();
          } else {
            this.appC.showErrToast(res.transferObjectMap.message);
          }
      }, error => {
              this.spinnerService.hide()
        });
    }

    smsVerify() {
      let data = {
          "eventExternal": {
              "name": "request_sms_verify",
              "key": "mykey"
          },
          "transferObjectMap": {
              "gatewayrequest": {
                  "code": this.smsOtp,
                  "token": this.token,
              }
          }
      }
      this.spinnerService.show();
      this.server.postApi('', data).subscribe(res => {
          this.spinnerService.hide()
          if (res.transferObjectMap.statusCode == 200) {
              localStorage.setItem('token', this.token);
              localStorage.setItem("header",JSON.stringify(true));
              this.header.loginToken = false;
              this.appC.showSuccToast(res.transferObjectMap.status);
              $("#sms-verify").modal('hide');
              let data = {
                messageType:"userUpdated",
                token: this.token
              }
              this.server.wsExchange.send(JSON.stringify(data));
              this.router.navigate(['/header/viewprofile']);
          }
          else {
            this.appC.showErrToast(res.transferObjectMap.message);
          }
      }, error => {
              this.spinnerService.hide()
      });
  
    }

    /**To resend otp */
    resend(){
      /**otp verfication  */
      let smsNo = {
          "eventExternal": {
              "name":"request_sms_auth",
              "key":"mykey"
          },
          "transferObjectMap": {
              "gatewayrequest": {
                  "phoneCountryCode": "",
                  "phone": "",
                  "token":localStorage.getItem('token')
              }
          }   
      }
      this.spinnerService.show();
      this.server.postApi('', smsNo).subscribe(response => {
          this.spinnerService.hide();
          if (response.transferObjectMap.statusCode == 200) {
              this.appC.showSuccToast(response.transferObjectMap.message);
              this.smsOtp = '';
          } else if (response.transferObjectMap.statusCode == 403) {
              this.header.logout();
          } else {
              this.smsOtp = '';
              this.appC.showErrToast(response.transferObjectMap.message);
          }
      }, error => {
          this.appC.showErrToast('Something went wrong');
          this.spinnerService.hide();
      });
    }



}

import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { AppComponent } from '../../../app.component';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';
import { ServiceService } from '../../../service/service.service';
declare var $: any;

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.css']
})
export class SignupComponent implements OnInit {
    signupForm: FormGroup;
    recaptcha: any = '';
    countryData: any;
    myCode: any;
    stateArry: any = []
    stateAll: any;
    Allcountry: any;
    countryArry: any = []
    countryall: any;

    constructor(private router: Router,private server: ServiceService, private appC: AppComponent, private spinnerService: Ng4LoadingSpinnerService,) { }

    ngOnInit() {
        $("#phoneNum").intlTelInput({
            autoPlaceholder: true,
            autoFormat: false,
            autoHideDialCode: false,
            initialCountry: 'in',
            nationalMode: false,
            onlyCountries: [],
            preferredCountries: ["us"],
            formatOnInit: true,
            separateDialCode: true
        });
        window.scrollTo(0,0);
        this.checkInputs();
        this.countryselect();
    }

    /** Function to validate form inputs */
    checkInputs() {
        this.signupForm = new FormGroup ({
            name: new FormControl('', [Validators.required, Validators.minLength(2),Validators.pattern(/^[a-z ,.'-]+$/i)]),
            email: new FormControl('', [Validators.required, Validators.pattern(/^[A-Z0-9_-]+([\.][A-Z0-9_]+)*@[A-Z0-9-]+(\.[a-zA-Z]{2,3})+$/i)]),
            password: new FormControl('', [Validators.required, Validators.pattern(/^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})/i)]),
            phone: new FormControl('', [Validators.required, Validators.pattern(/^[1-9]{1}[0-9]*$/), Validators.minLength(7)]),
            country: new FormControl('',[Validators.required]),
            state: new FormControl('',[Validators.required]),
            privacyPolicy: new FormControl('', [Validators.required]),
            terms: new FormControl('', [Validators.required]),
            responsibility: new FormControl('', [Validators.required]),
            confirmPassword: new FormControl('', [Validators.required]),
        }, passwordMatchValidator);
        /** Function for password match and mismatch */
        function passwordMatchValidator(g: FormGroup) {
            let pass = g.get('password').value;
            let confPass = g.get('confirmPassword').value;
            if (pass != confPass) {
                g.get('confirmPassword').setErrors({ mismatch: true });
            } else {
                g.get('confirmPassword').setErrors(null)
                return null
            }
        }
    }

    /** to get the value of field  */
    get name(): any {
        return this.signupForm.get('name');
    }
    get email(): any {
      return this.signupForm.get('email');
    }
    get password(): any {
        return this.signupForm.get('password');
    }
    get confirmPassword(): any {
        return this.signupForm.get('confirmPassword');
    }
    get country(): any {
        return this.signupForm.get('country');
    }
    get state(): any {
        return this.signupForm.get('state');
    }  
    get privacyPolicy(): any {
      return this.signupForm.get('privacyPolicy');
    }
    get terms(): any {
      return this.signupForm.get('terms');
    }
    get responsibility(): any {
      return this.signupForm.get('responsibility');
    }
    get phone(): any {
        return this.signupForm.get('phone');
      }

    login() {
      this.router.navigateByUrl('header/login');
    }

    resolved(e) {
      if(e)
          this.recaptcha = e;
      else{
          this.recaptcha = "";
      }
    }

    register()  {
      this.countryData = $("#phoneNum").intlTelInput("getSelectedCountryData");
      this.myCode = this.countryData.dialCode;
      let signupData = {
          "eventExternal": {
              "name": "request_signup",
              "key": "mykey"
          },
          "transferObjectMap": {
              "gatewayrequest": {
                  "firstName": this.signupForm.value.name,
                  "phoneCountryCode": this.myCode,
                  "phone": this.signupForm.value.phone,
                  "email": this.signupForm.value.email,
                  "password": this.signupForm.value.password,
                  "country": this.signupForm.value.country,
                  "state": this.signupForm.value.state,
                  "url": "http://162.222.32.20:1487/header/twofa"
                 //"url": "http://172.16.6.232:4200/header/twofa"
                 
              }
          }
      }
      this.spinnerService.show();
      this.server.postApi('', signupData).subscribe(response => {
          this.spinnerService.hide();
          if (response.transferObjectMap.statusCode == 200) {
              this.appC.showSuccToast(response.transferObjectMap.message)
              localStorage.setItem('email',this.signupForm.value.email);
              this.router.navigateByUrl('header/emailVerify/signup');
          } else {
              this.appC.showErrToast(response.transferObjectMap.message);
          }
      }, error => {
          this.spinnerService.hide();
          this.appC.showErrToast('Something went wrong');
      });
    }

    statechange(state) {
        this.stateAll = state
    }

    countrychange(country) {
        this.Allcountry = country
        let cuntrydata = {
        "eventExternal": {
            "name": "request_statebycountrylist",
            "key": "mykey"
        },
        "transferObjectMap": {
            "gatewayrequest":{
                "country": this.Allcountry
    
            }
        }
        }
        this.spinnerService.show()
        /** get API call */
        this.server.postApi('', cuntrydata).subscribe(response => {
        this.spinnerService.hide()
        if (response.transferObjectMap.statusCode == 200) {
            this.stateArry = response.transferObjectMap.result
    
        } else {
            this.appC.showErrToast(response.transferObjectMap.message)
        }
    
        }, error => {
        this.spinnerService.hide();
        this.appC.showErrToast("Something went wrong")
        })
    }
    
    countryselect() {
        let cuntrydata = {
            "eventExternal":{
                "name": "request_countrylist",
                "key": "mykey"
            },
            "transferObjectMap": {
                "gatewayrequest":{}
            }
        }
        this.spinnerService.show()
        /** get API call */
        this.server.postApi('', cuntrydata).subscribe(response => {
        this.spinnerService.hide()
        if (response.transferObjectMap.statusCode == 200) {
            this.countryArry = response.transferObjectMap.result
    
        } else {
            this.appC.showErrToast(response.transferObjectMap.message)
        }
    
        }, error => {
        this.spinnerService.hide();
        this.appC.showErrToast("Something went wrong")
        })
    }
    

}

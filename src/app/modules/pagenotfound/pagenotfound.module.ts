import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagenotfoundComponent } from './pagenotfound/pagenotfound.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [PagenotfoundComponent]
})
export class PagenotfoundModule { }

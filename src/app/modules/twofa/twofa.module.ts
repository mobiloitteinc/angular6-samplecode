import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TwofaComponent } from './twofa/twofa.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule
  ],
  declarations: [TwofaComponent]
})
export class TwofaModule { }

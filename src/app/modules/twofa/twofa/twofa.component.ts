import { Component, OnInit } from '@angular/core';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Router } from '@angular/router';
import { ServiceService } from '../../../service/service.service';
import { AppComponent } from '../../../app.component';
import { HeaderComponent } from '../../header/header/header.component';
declare var $: any;

@Component({
  selector: 'app-twofa',
  templateUrl: './twofa.component.html',
  styleUrls: ['./twofa.component.css']
})
export class TwofaComponent implements OnInit {

  googleOtp: any;
  page: string;
  secretKey: any;
  angularxQrCode: string;
  smsOtp: any;

  constructor(private router: Router,private server: ServiceService, private appC: AppComponent, private spinnerService: Ng4LoadingSpinnerService, public header: HeaderComponent) { }

    ngOnInit() {
            let url = window.location.href.split('/')
            this.page = url[url.length-1];
            this.checkOrigin();
            window.scrollTo(0,0);
    }

    checkOrigin() {
        let url = window.location.href.split('/')
        this.page = url[url.length-1];
        let token = localStorage.getItem('noAuth')
        if(token) {
        } else {
            let data =
            {
            "eventExternal": {
                "name": "request_verify_user_contact",
                "key": "mykey"
            },
            "transferObjectMap": {
                "gatewayrequest": {
                "token": this.page
                }
            }
            }
        this.spinnerService.show();
        this.server.postApi('', data).subscribe(res => {
            this.spinnerService.hide();
            if (res.transferObjectMap.statusCode == 200) {
                this.appC.showSuccToast(res.transferObjectMap.message)
            } else {
                this.appC.showErrToast(res.transferObjectMap.message);
            }
        }, error => {
                this.spinnerService.hide()
            });
    }
    }

    openGoogleAuth() {
        //$('#key').modal('show')
        this.googleOtp = "" 
        let data = {
            "eventExternal": {
                "name": "request_google_auth",
                "key": "mykey"
            },
            "transferObjectMap": {
                "gatewayrequest": {
                    "token": this.page
                }
            }
        }
        this.spinnerService.show();
        this.server.postApi('', data).subscribe(res => {
            this.spinnerService.hide();
            if (res.transferObjectMap.statusCode == 200) {
                this.appC.showSuccToast(res.transferObjectMap.status);
                this.angularxQrCode="data:image/png;base64,"+res.transferObjectMap.QRData
                this.secretKey = res.transferObjectMap.secretKey;
                localStorage.setItem("secretKey",res.transferObjectMap.secretKey);
                $("#key").modal({ backdrop: 'static', keyboard: false });
            } else {
                this.appC.showErrToast(res.transferObjectMap.message);
            }
        }, error => {
            this.spinnerService.hide();
        });

    }

    googleVerify() {
        let data = {
        "eventExternal": {
            "name": "request_google_verify",
            "key": "mykey"
        },
        "transferObjectMap": {
            "gatewayrequest": {
                "token": this.page,
                "otp": this.googleOtp,
                "secretKey": this.secretKey,
                "clientTime": new Date().getTime()
            }
        }
        }
        this.spinnerService.show();
        this.server.postApi('', data).subscribe(res => {
            this.spinnerService.hide();
            if (res.transferObjectMap.statusCode == 200) {
                //localStorage.setItem("token",res.transferObjectMap.gatewayrequest.token);
                localStorage.setItem("token",this.page);
                localStorage.setItem("header",JSON.stringify(true));
                this.header.loginToken = false;
                this.appC.showSuccToast(res.transferObjectMap.status);
                $("#key").modal('hide');
                localStorage.setItem('userID', res.transferObjectMap.result[0].userId);
                let data = {
                    messageType:"userUpdated",
                    token: this.page
                }
                this.server.wsExchange.send(JSON.stringify(data));
                this.router.navigate(['/header/viewprofile']);
            }
            else {
                this.appC.showErrToast(res.transferObjectMap.message);
            }
        }, error => {
            this.spinnerService.hide();
        });
    }

    openotpmodal() {
        //$("#sms-verify").modal({ backdrop: 'static', keyboard: false });
        this.smsOtp = "";
        let data = {
            "eventExternal": {
                "name": "request_sms_auth",
                "key": "mykey"
            },
            "transferObjectMap": {
                "gatewayrequest": {
                    "token": this.page
                }
            }
        }
        this.spinnerService.show();
        this.server.postApi('', data).subscribe(res => {
            this.spinnerService.hide();
            if (res.transferObjectMap.statusCode == 200) {
                this.appC.showSuccToast(res.transferObjectMap.status);
                // this.resendButton = false;
                $("#sms-verify").modal({ backdrop: 'static', keyboard: false });
                // this.timer();
            } else {
                this.appC.showErrToast(res.transferObjectMap.message);
            }
        }, error => {
                this.spinnerService.hide()
            });
    }

    smsVerify() {
        let data = {
            "eventExternal": {
                "name": "request_sms_verify",
                "key": "mykey"
            },
            "transferObjectMap": {
                "gatewayrequest": {
                    "code": this.smsOtp,
                    "token": this.page
                }
            }
        }
        this.spinnerService.show();
        this.server.postApi('', data).subscribe(res => {
            this.spinnerService.hide()
            if (res.transferObjectMap.statusCode == 200) {
                //localStorage.setItem("token",res.transferObjectMap.gatewayrequest.token);
                localStorage.setItem("token",this.page);
                localStorage.setItem("header",JSON.stringify(true));
                this.header.loginToken = false;
                this.appC.showSuccToast(res.transferObjectMap.status);
                $("#sms-verify").modal('hide');
                localStorage.setItem('userID', res.transferObjectMap.result[0].userId);
                    let data = {
                        messageType:"userUpdated",
                        token: this.page
                    }
                    this.server.wsExchange.send(JSON.stringify(data));
                    this.router.navigate(['/header/viewprofile']);
            }
            else {
                this.appC.showErrToast(res.transferObjectMap.message);
            }
        }, error => {
                this.spinnerService.hide()
        });
    
    }

    /**To resend otp */
    resend(){
        /**otp verfication  */
        let smsNo = {
            "eventExternal": {
                "name":"request_sms_auth",
                "key":"mykey"
            },
            "transferObjectMap": {
                "gatewayrequest": {
                    "phoneCountryCode": "",
                    "phone": "",
                    "token":localStorage.getItem('token')
                }
            }   
        }
        this.spinnerService.show();
        this.server.postApi('', smsNo).subscribe(response => {
            this.spinnerService.hide();
            if (response.transferObjectMap.statusCode == 200) {
                this.appC.showSuccToast(response.transferObjectMap.message);
                this.smsOtp = '';
            } else if (response.transferObjectMap.statusCode == 403) {
                this.header.logout();
            } else {
                this.smsOtp = '';
                this.appC.showErrToast(response.transferObjectMap.message);
            }
        }, error => {
            this.appC.showErrToast('Something went wrong');
            this.spinnerService.hide();
        });
    }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { EmailverifyComponent } from './emailverify/emailverify.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [EmailverifyComponent]
})
export class EmailverifyModule { }

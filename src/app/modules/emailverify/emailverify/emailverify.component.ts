import { Component, OnInit } from '@angular/core';
import { Ng4LoadingSpinnerService } from '../../../../../node_modules/ng4-loading-spinner';
import { AppComponent } from '../../../app.component';
import { HeaderComponent } from '../../header/header/header.component';
import { ServiceService } from '../../../service/service.service';

@Component({
  selector: 'app-emailverify',
  templateUrl: './emailverify.component.html',
  styleUrls: ['./emailverify.component.css']
})
export class EmailverifyComponent implements OnInit {

  email: any;
  page: any = "";

  constructor(private server: ServiceService, private appC: AppComponent, private spinnerService: Ng4LoadingSpinnerService, public header:HeaderComponent) { }

    ngOnInit() {
      window.scrollTo(0,0)
      let url = window.location.href.split('/')
      this.page = url[url.length - 1]
      if(this.page == 'login')  {
        this.page = true;
      }
      else 
      this.page = false;

      
    }
    

    resendEmail() {     
            let data = {
                  "eventExternal": {
                        "name": "request_verify_user_email",
                        "key": "mykey"
                  },
                  "transferObjectMap": {
                        "gatewayrequest": {
                              "email":localStorage.getItem("email"),
                              "url": "http://162.222.32.20:1487/header/twofa",
                              //"url": "http://172.16.6.212:4200/header/twofa"
                        }
                  }
            }
            this.spinnerService.show();
            this.server.postApi('', data).subscribe(response => {
            this.spinnerService.hide();
            if (response.transferObjectMap.statusCode == 200) {
              this.appC.showSuccToast(response.transferObjectMap.message);  
            } else if(response.transferObjectMap.statusCode == 403){
              this.appC.showErrToast(response.transferObjectMap.message); 
              //this.header.tokenExpire();
            } else {
              this.appC.showErrToast(response.transferObjectMap.message);
            }
            } , error => {
            this.spinnerService.hide();
            this.appC.showErrToast('Something went wrong');
            });        
    }


}

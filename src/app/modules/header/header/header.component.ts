import { Component, OnInit, Renderer2 } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../../../app.component';
import { ServiceService } from '../../../service/service.service';
import { Ng4LoadingSpinnerService } from 'ng4-loading-spinner';
import { Observable, Subject } from 'rxjs';
declare var $: any;

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
    loginToken: boolean;
    name: any;
    userImage: string;
    private subject = new Subject<any>();
    
    constructor(private router: Router,private server: ServiceService, private appC: AppComponent, private spinnerService: Ng4LoadingSpinnerService, private renderer: Renderer2) { }

    ngOnInit() {
        if(localStorage.getItem("token")) {
            this.loginToken = false;
        } else {
            this.loginToken = true;
        }
        this.getprofile();
        window.scrollTo(0,0);
    }

    /** Function for call event in child component */
    fireToChild(): Observable<any> {
        return this.subject.asObservable();
    }

    goToPage(val) {
        switch(val) {
            case 1: 
                this.router.navigateByUrl('header/login');
                break;
            case 2: 
                this.router.navigateByUrl('header/signup');
                break;
            case 3:
                this.router.navigateByUrl('header/viewprofile');
                break;
            case 4:
                this.router.navigateByUrl('header/exchange');
                break;
            case 5:
                this.router.navigateByUrl('/header/deposit');
                break;
            case 6:
                this.router.navigateByUrl('/header/withdraw');
                break;
            case 7:
                this.router.navigateByUrl('/header/suggestion');
                break;
            case 8:
                this.router.navigateByUrl('/header/changepassword');
                break;
            case 9:
                this.router.navigateByUrl('/header');
                break;
            case 10:
                this.router.navigateByUrl('/header/order');
                break;
            case 11:
                this.router.navigateByUrl('/header/wallet');
                break;
            case 12:
                this.router.navigateByUrl('/header/otc');
                break;
            case 13:
                this.router.navigateByUrl('/header/aboutUs');
                break;
            case 14:
                this.router.navigateByUrl('/header/privacyPolicy');
                break;
            case 15:
                this.router.navigateByUrl('/header/termsConditions');
                break;
        }
    }

    logout() {
        let data = {
            "eventExternal": {
                    "name":"request_logout",
                    "key":"mykey"
                },
            "transferObjectMap": {
                "gatewayrequest": {
                    "token":localStorage.getItem("token")
                }
            }
        }
        this.spinnerService.show();
        this.server.postApi('', data).subscribe(response => {
            this.spinnerService.hide();
            if (response.transferObjectMap.statusCode == 200 || response.transferObjectMap.statusCode == 403) {
                localStorage.clear();
                this.loginToken = true;
                $('#logout').modal('hide');
                let wsData = {
                    messageType:"userUpdated",
                    token: "notLoggedIn"
                }
                this.server.wsExchange.send(JSON.stringify(wsData));
                this.router.navigateByUrl('');
            } else {
                this.appC.showErrToast(response.transferObjectMap.message);
            }
        }, error => {
            this.spinnerService.hide();
        });
    }

    cancel() {
        $('#logout').modal('hide');
    }

    getprofile() {
        if(localStorage.getItem('token')) {
            this.server.getApi("?key="+localStorage.getItem('token')+"USER").subscribe((succ) => {
                this.name = succ[0].firstName;  
                if(succ[0].userImage==null || succ[0].userImage=='')
                    this.userImage = 'assets/images/user-profile.png';
                else
                    this.userImage = succ[0].userImage;
            }, (err) => {
            });
        }
    }

    /** Function for theme change */
    changeThemeMode(e) {
        if(e) {
            this.renderer.addClass(document.body, 'night-mode');
        } else {
            this.renderer.removeClass(document.body, 'night-mode');
        }
        this.subject.next({ text: "modeChange" });
    } 
}

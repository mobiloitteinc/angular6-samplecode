import { Component } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Router, NavigationEnd } from '@angular/router';
import { ServiceService } from './service/service.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'app';

    constructor(private toasterService: ToastrService, public router: Router, public server : ServiceService){}

    /** Function to show success toast */
    showSuccToast(msg) {    
        this.toasterService.success(msg, "Bithood");
    }

    /** Function to show error toast */
    showErrToast(msg) {
        this.toasterService.error(msg, "Bithood");
    }

    /** Function to show warning toast */
    showWarnToast(msg) {
        this.toasterService.warning(msg, "Bithood");
    }

    /** Function to show info toast */
    showInfoToast(msg) {
        this.toasterService.info(msg, "Bithood");
    }

    ngOnInit() {
        this.initWsSocket();
        this.router.events.subscribe(x => {
            if (x instanceof NavigationEnd) {
                if (localStorage.getItem('token') != null) {
                    if ((x.url == '/header/login' || x.url == '/header/signup' || x.url == '/header/forgetpassword')) {
                        this.router.navigate(['']);
                    }
                }
                else {
                    if ( (x.url == '/header/viewprofile') || (x.url == '/header/changepassword') || (x.url == '/header/order') || (x.url == '/header/deposit')|| (x.url == '/header/profile')|| (x.url == '/header/withdraw')|| (x.url == '/header/withdrawcrypto')) {
                        this.router.navigate([''])
                    }
                }
            }
        })
    }

    /** Function to init ws */
    initWsSocket() {
        localStorage.getItem("token") ? this.server.initSocket(localStorage.getItem("token")) : this.server.initSocket("notLoggedIn");
    }
}

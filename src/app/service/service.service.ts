import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';

@Injectable()
export class ServiceService {
    //baseUrl = 'http://172.16.21.20:8086/bithood/rest/gateway'; 
    baseUrl = 'http://182.76.185.46:8086/bithood/rest/gateway'; 
    //baseUrl = 'http://172.16.2.4:8080/GatewayService/rest/gateway';
    wsExchange : any;
    constructor(private http:HttpClient) { }

    /** method declaration */
    getApi(url): Observable<any> {
      return this.http.get(this.baseUrl + url);
    }

    /** Function for post method */
    postApi(url, data): Observable<any> {
        return this.http.post(this.baseUrl + url, data);
    }

    initSocket(val) {
        this.wsExchange = new WebSocket("ws://172.16.21.20:8086/bithood/socket/"+val);
        this.wsExchange.addEventListener('open', function (event) {
            console.log('ws connected for exchange');
        });
        this.wsExchange.addEventListener('close', function (event) {
            console.log('ws disconnected for exchange');
        });
    }

}

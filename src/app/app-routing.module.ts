import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HeaderComponent } from './modules/header/header/header.component';
import { HomeComponent } from './modules/home/home/home.component';
import { LoginComponent } from './modules/login/login/login.component';
import { SignupComponent } from './modules/signup/signup/signup.component';
import { EmailverifyComponent } from './modules/emailverify/emailverify/emailverify.component';
import { ViewprofileComponent } from './modules/viewprofile/viewprofile/viewprofile.component';
import { EditprofileComponent } from './modules/editprofile/editprofile/editprofile.component';
import { TwofaComponent } from './modules/twofa/twofa/twofa.component';
import { ChangepasswordComponent } from './modules/changepassword/changepassword/changepassword.component';
import { ResetpasswordComponent } from './modules/resetpassword/resetpassword/resetpassword.component';
import { PagenotfoundComponent } from './modules/pagenotfound/pagenotfound/pagenotfound.component';


const routes: Routes = [
  { path: '', redirectTo: 'header', pathMatch: 'full'},
  { path: 'header', component: HeaderComponent, 
        children: [
            { path: '', component: HomeComponent},
            { path: 'login', component: LoginComponent},
            { path: 'signup', component: SignupComponent},
            { path: 'emailVerify/:token', component: EmailverifyComponent},
            { path: 'viewprofile', component: ViewprofileComponent},
            { path: 'editprofile', component: EditprofileComponent},
            { path: 'twofa/:token', component: TwofaComponent},
            { path: 'reset/:token', component: ResetpasswordComponent},
            { path: 'changepassword', component: ChangepasswordComponent},
            { path: '**', component: PagenotfoundComponent}
        ],
  },
  { path: '**', component: PagenotfoundComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginModule } from './modules/login/login.module';
import { SignupModule } from './modules/signup/signup.module';
import { ForgetpasswordModule } from './modules/forgetpassword/forgetpassword.module';
import { ResetpasswordModule } from './modules/resetpassword/resetpassword.module';
import { TwofaModule } from './modules/twofa/twofa.module';
import { HeaderModule } from './modules/header/header.module';
import { HomeModule } from './modules/home/home.module';
import { ToastrModule } from 'ngx-toastr';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Ng4LoadingSpinnerModule } from 'ng4-loading-spinner';
import { RecaptchaModule } from 'ng-recaptcha';
import { ServiceService } from './service/service.service';
import { HttpClientModule } from '@angular/common/http';
import { EmailverifyModule } from './modules/emailverify/emailverify.module';
import { ViewprofileModule } from './modules/viewprofile/viewprofile.module';
import { EditprofileModule } from './modules/editprofile/editprofile.module';
import { QRCodeModule } from 'angularx-qrcode';
import { ChangepasswordModule } from './modules/changepassword/changepassword.module';
import * as $ from 'jquery';
import { PagenotfoundModule } from './modules/pagenotfound/pagenotfound.module';
import { FilterdataPipe } from './filterdata.pipe';





@NgModule({
  declarations: [
    AppComponent,
    FilterdataPipe,

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    LoginModule,
    SignupModule,
    ForgetpasswordModule,
    ResetpasswordModule,
    TwofaModule,
    HeaderModule,
    HomeModule,
    ToastrModule.forRoot(),
    BrowserAnimationsModule,
    Ng4LoadingSpinnerModule.forRoot(),
    RecaptchaModule.forRoot(), 
    HttpClientModule,
    EmailverifyModule,
    ViewprofileModule,
    EditprofileModule,
    TwofaModule,
    QRCodeModule,
    ChangepasswordModule,
    PagenotfoundModule,

    

  ],
  providers: [ServiceService],
  bootstrap: [AppComponent]
})
export class AppModule { }
